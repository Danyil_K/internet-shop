<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\ProductTypeController;
use App\Http\Controllers\CardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SignController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [HomeController::class, 'getProducts']);

# Default frontend
Route::get('/get_products', [HomeController::class, 'getProducts']);
Route::get('/get_brands', [HomeController::class, 'getBrands']);
Route::get('/get_product-types', [HomeController::class, 'getProductTypes']);
Route::get('/get_brand/{brand}', [HomeController::class, 'getByBrandName']);
Route::get('/get_type/{type}', [HomeController::class, 'getByProductType']);

# Registration and logging
Route::prefix('auth')->group(function(){
    Route::post('/register', [SignController::class, 'register']);
    Route::post('/login', [SignController::class, 'login']);
});

# User
Route::group(['prefix' => 'user', 'middleware' => 'auth:api'], function() {
    Route::post('/logout', [SignController::class, 'logout']);
    Route::get('/get_user', [SignController::class, 'getUser']);

    Route::post('/update', [SignController::class, 'update']);
    Route::get('/get_recommendations/{id}', [CardController::class, 'getRecommendations']);
});

# Admin
Route::group(['prefix' => 'admin', 'middleware' => ['auth:api', 'admin']], function() {
    Route::prefix('card')->group(function(){
        # get info about user
        // Route::get('/get_user', [SignController::class, 'user']);
        # for admin info
        Route::post('/', [CardController::class, 'create']);
        Route::get('/get_products', [CardController::class, 'getProducts']);
        Route::get('/get_brands', [BrandController::class, 'getBrands']);
        Route::get('/get_brands/{id}', [BrandController::class, 'getOneBrand']);
        Route::get('/get_brands_concat/', [BrandController::class, 'getIdAndNameBrands']);
        Route::get('/get_brands_cards/', [BrandController::class, 'getBrandsWithCards']);
        Route::get('/get_product-types', [ProductTypeController::class, 'getProductTypes']);
        Route::get('/get_brand/{brand}', [CardController::class, 'getByBrandName']);
        Route::get('/get_type/{type}', [CardController::class, 'getByProductType']);
        Route::post('/update_card/{id}', [CardController::class, 'updateCard']);
        Route::delete('/delete_card/{id}', [CardController::class, 'deleteCard']);
    });
});
