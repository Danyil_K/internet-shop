<?php

use Illuminate\Database\Seeder;
use App\Card;
use App\Brand;
use App\Cover;
use App\Field;
use App\ProductType;

class CardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # 1
        Card::insert([
            'title' => 'Ноутбук Fujitsu-Siemens Amilo Pi2540 (RUS-110135-002)',
            'brief_information' => 'Екран 15.4" WXGA BrilliantView/ Pentium Dual Core T2390(1.86GHz) / 2Gb / 160GB / Radeon HD2400 / DVD Super Multi / WiFi / Vista Premium / 3.0kg.',
            'cost' => '15999',
            'additional_information' => 'Чипсет - Intel® 965PM+ICH8-M'
        ]);
            Field::insert(['name_field' => 'processor', 'description_field' => 'Двоядерний Intel Pentium Dual Core T2390 (1.86 ГГЦ)', 'card_id' => 1]);
            Brand::insert(['brand_name' => 'Fujitsu-Siemens', 'card_id' => 1]);
            ProductType::insert(['type' => 'notebook', 'card_id' => 1]);
            Cover::insert(['image' => 'Fujitsu1', 'card_id' => 1]);

        # 2
        Card::insert([
            'title' => 'Ноутбук Acer Aspire 5 A515-54G-502N (NX.HVGEU.006) Pure Silver',
            'brief_information' => 'Екран 15.6" IPS (1920x1080) Full HD, матовий / Intel Core i5-10210U (1.6 — 4.2 ГГц) / RAM 8 ГБ / SSD 512 ГБ / nVidia GeForce MX350, 2 ГБ / без ОД / LAN / Wi-Fi / Bluetooth / вебкамера / без ОС / 1.8 кг / сріблястий.',
            'cost' => '18999',
            'additional_information' => 'Чипсет - Intel® 965PM+ICH8-M'
        ]);
            Field::insert(['name_field' => 'processor', 'description_field' => 'Двоядерний Intel Pentium Dual Core T2390 (1.86 ГГЦ)', 'card_id' => 2]);
            Brand::insert(['brand_name' => 'Acer', 'card_id' => 2]);
            ProductType::insert(['type' => 'notebook', 'card_id' => 2]);
            Cover::insert(['image' => 'Acer1', 'card_id' => 2]);

        # 3
        Card::insert([
            'title' => 'Ноутбук Asus ROG Strix G15 G512LI-HN094 (90NR0381-M01620) Black',
            'brief_information' => 'Екран 15.6" IPS (1920x1080) Full HD 144 Гц, матовий / Intel Core i5-10300H (2.5 — 4.5 ГГц) / RAM 8 ГБ / SSD 512 ГБ / nVidia GeForce GTX 1650 Ti, 4 ГБ / без ОД / LAN / Wi-Fi / Bluetooth / без ОС / 2.39 кг / чорний.',
            'cost' => '21999',
            'additional_information' => 'Чипсет - Intel® 965PM+ICH8-M'
        ]);
            Field::insert(['name_field' => 'processor', 'description_field' => 'Двоядерний Intel Pentium Dual Core T2390 (1.86 ГГЦ)', 'card_id' => 3]);
            Brand::insert(['brand_name' => 'Asus', 'card_id' => 3]);
            ProductType::insert(['type' => 'notebook', 'card_id' => 3]);
            Cover::insert(['image' => 'Asus1', 'card_id' => 3]);

        # 4
        Card::insert([
            'title' => 'Ноутбук HP Pavilion Gaming 15-dk0001ua (1S8B9EA) Dark Grey',
            'brief_information' => 'Екран 15.6" IPS (1920x1080) Full HD, матовий / Intel Core i5-9300H (2.4 — 4.1 ГГц) / RAM 8 ГБ / SSD 256 ГБ / nVidia GeForce GTX 1650, 4 ГБ / без ОД / LAN / Wi-Fi / Bluetooth / вебкамера / FreeDOS / 2.23 кг / темно-сірий.',
            'cost' => '20999',
            'additional_information' => 'Чипсет - Intel® 965PM+ICH8-M'
        ]);
            Field::insert(['name_field' => 'processor', 'description_field' => 'Двоядерний Intel Pentium Dual Core T2390 (1.86 ГГЦ)', 'card_id' => 4]);
            Brand::insert(['brand_name' => 'HP', 'card_id' => 4]);
            ProductType::insert(['type' => 'notebook', 'card_id' => 4]);
            Cover::insert(['image' => 'HP1', 'card_id' => 4]);

        # 5
        Card::insert([
            'title' => 'Ноутбук Lenovo V15-IIL (82C50057RA) Iron Grey',
            'brief_information' => 'Екран 15.6" TN (1920x1080) Full HD, матовий / Intel Core i5-1035G1 (1.0 — 3.6 ГГц) / RAM 8 ГБ / SSD 256 ГБ / Intel UHD / без ОД / Wi-Fi / Bluetooth / вебкамера / без ОС / 1.85 кг / сірий.',
            'cost' => '16599',
            'additional_information' => 'Чипсет - Intel® 965PM+ICH8-M'
        ]);
            Field::insert(['name_field' => 'processor', 'description_field' => 'Двоядерний Intel Pentium Dual Core T2390 (1.86 ГГЦ)', 'card_id' => 5]);
            Brand::insert(['brand_name' => 'Lenovo', 'card_id' => 5]);
            ProductType::insert(['type' => 'notebook', 'card_id' => 5]);
            Cover::insert(['image' => 'Lenovo1', 'card_id' => 5]);

        # 6
        Card::insert([
            'title' => 'Ноутбук MSI Modern 14 B4MW Luxury Black',
            'brief_information' => 'Екран 14" IPS (1920x1080) Full HD, матовий / AMD Ryzen 5 4500U (2.3 — 4.0 ГГц) / RAM 8 ГБ / SSD 256 ГБ / AMD Radeon Graphics / без ОД / Wi-Fi / Bluetooth / вебкамера / DOS / 1.3 кг / чорний.',
            'cost' => '18999',
            'additional_information' => 'Чипсет - Intel® 965PM+ICH8-M'
        ]);
            Field::insert(['name_field' => 'processor', 'description_field' => 'Двоядерний Intel Pentium Dual Core T2390 (1.86 ГГЦ)', 'card_id' => 6]);
            Brand::insert(['brand_name' => 'MSI', 'card_id' => 6]);
            ProductType::insert(['type' => 'notebook', 'card_id' => 6]);
            Cover::insert(['image' => 'MSI1', 'card_id' => 6]);

        # 7
        Card::insert([
            'title' => 'Ноутбук Acer Swift 3 SF314-58-52DU (NX.HPMEU.00L) Sparkly Silver',
            'brief_information' => 'Екран 14" IPS (1920x1080) Full HD, матовий / Intel Core i5-10210U (1.6 - 4.2 ГГц) / RAM 8 ГБ / SSD 512 ГБ / Intel UHD Graphics / без ОД / Wi-Fi / Bluetooth / веб-камера / без ОС / 1.6 кг / сріблястий.',
            'cost' => '18999',
            'additional_information' => 'Чипсет - Intel® 965PM+ICH8-M'
        ]);
            Field::insert(['name_field' => 'processor', 'description_field' => 'Двоядерний Intel Pentium Dual Core T2390 (1.86 ГГЦ)', 'card_id' => 7]);
            Brand::insert(['brand_name' => 'Acer', 'card_id' => 7]);
            ProductType::insert(['type' => 'notebook', 'card_id' => 7]);
            Cover::insert(['image' => 'Acer2', 'card_id' => 7]);

        # 8
        Card::insert([
            'title' => 'Ноутбук Dell Inspiron 3593 (3593Fi58S2IUHD-LBK) Black',
            'brief_information' => 'Екран 15.6" TN (1920x1080) Full HD, глянсовий з антивідблисковим покриттям / Intel Core i5-1035G1 (1.0 — 3.6 ГГц) / RAM 8 ГБ / SSD 256 ГБ / Intel UHD Graphics 620 / без ОД / Wi-Fi / Bluetooth / вебкамера / Linux / 2.2 кг / чорний.',
            'cost' => '17999',
            'additional_information' => 'Чипсет - Intel® 965PM+ICH8-M'
        ]);
            Field::insert(['name_field' => 'processor', 'description_field' => 'Двоядерний Intel Pentium Dual Core T2390 (1.86 ГГЦ)', 'card_id' => 8]);
            Brand::insert(['brand_name' => 'Dell', 'card_id' => 8]);
            ProductType::insert(['type' => 'notebook', 'card_id' => 8]);
            Cover::insert(['image' => 'Dell1', 'card_id' => 8]);

        # 9
        Card::insert([
            'title' => 'Ноутбук Asus ROG Strix G15 G512LU-HN093 (90NR0351-M02460) Black',
            'brief_information' => 'Екран 15.6" IPS (1920x1080) Full HD 144 Гц, матовий / Intel Core i7-10750H (2.6 — 5.0 ГГц) / RAM 16 ГБ / SSD 512 ГБ / nVidia GeForce GTX 1660 Ti, 6 ГБ / без ОД / LAN / Wi-Fi / Bluetooth / без ОС / 2.4 кг / чорний.',
            'cost' => '35999',
            'additional_information' => 'Чипсет - Intel® 965PM+ICH8-M'
        ]);
            Field::insert(['name_field' => 'processor', 'description_field' => 'Двоядерний Intel Pentium Dual Core T2390 (1.86 ГГЦ)', 'card_id' => 9]);
            Brand::insert(['brand_name' => 'Asus', 'card_id' => 9]);
            ProductType::insert(['type' => 'notebook', 'card_id' => 9]);
            Cover::insert(['image' => 'Asus2', 'card_id' => 9]);

        # 10
        Card::insert([
            'title' => 'Ноутбук Acer Aspire 7 A715-75G-57LR (NH.Q87EU.006) Charcoal Black',
            'brief_information' => 'Екран 15.6" IPS (1920x1080) Full HD, матовий / Intel Core i5-9300H (2.4 — 4.1 ГГц) / RAM 16 ГБ / SSD 512 ГБ / nVidia GeForce GTX 1650, 4 ГБ / без ОД / LAN / Wi-Fi / Bluetooth / вебкамера / без ОС / 2.15 кг / чорний.',
            'cost' => '24999',
            'additional_information' => 'Чипсет - Intel® 965PM+ICH8-M'
        ]);
            Field::insert(['name_field' => 'processor', 'description_field' => 'Двоядерний Intel Pentium Dual Core T2390 (1.86 ГГЦ)', 'card_id' => 10]);
            Brand::insert(['brand_name' => 'Acer', 'card_id' => 10]);
            ProductType::insert(['type' => 'notebook', 'card_id' => 10]);
            Cover::insert(['image' => 'Acer3', 'card_id' => 10]);
    }
}
