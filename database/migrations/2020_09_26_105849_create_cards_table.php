<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->bigIncrements('card_id');
            $table->string('title', 200);
            $table->string('brief_information', 450);
            $table->string('cost', 10);
            // $table->string('processor', 150)->nullable();
            // $table->string('operational_memory', 15)->nullable();
            // $table->string('display', 100)->nullable();
            // $table->string('operational_system', 100)->nullable();
            // $table->string('processor_generation', 100)->nullable();
            // $table->string('num_of_slots_m_2', 100)->nullable();
            // $table->string('color', 100)->nullable();
            // $table->string('screen_refresh_rate', 100)->nullable();
            // $table->string('keyboard', 100)->nullable();
            // $table->string('optical_drive', 100)->nullable();
            // $table->string('drive_volume', 100)->nullable();
            // $table->string('ukrainian_keyboard_layout', 100)->nullable();
            // $table->string('num_of_slots_ram', 100)->nullable();
            // $table->string('weight', 100)->nullable();
            // $table->string('battery', 100)->nullable();
            // $table->string('type_ram', 100)->nullable();
            // $table->string('additional_features', 100)->nullable();
            // $table->string('graphics_adapter', 200)->nullable();
            // $table->string('network_adapters', 100)->nullable();
            // $table->string('connectors_and_ports', 300)->nullable();
            // $table->string('battery_characteristics', 100)->nullable();
            // $table->string('dimensions', 100)->nullable();
            // $table->string('from_country', 100)->nullable();
            // $table->string('delivery_set', 100)->nullable();
            // $table->string('country_brand_reg', 100)->nullable();
            // $table->string('guarantee', 100)->nullable();
            $table->string('additional_information', 450)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
