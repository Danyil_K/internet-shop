<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    protected $primaryKey = 'brand_id';

    protected $fillable = ['brand_id', 'brand_name', 'card_id'];

    /**
     * relation one to one with Products
    */
    public function cards()
    {
        return $this->belongsTo('App\Card', 'card_id');
    }
}
