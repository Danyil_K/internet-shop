<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Card extends Model
{
    protected $table = 'cards';

    protected $primaryKey = 'card_id';

    protected $fillable = ['card_id', 'title', 'brief_information', 'cost', 'additional_information'];

    /**
     * callable in controller for getting all products
    */
    public function getProductsAllInfo(Request $request, int $offset = NULL, int $limit = NULL)
    {
        $offset = $request->offset;
        $limit = $request->limit;

        # check have or not offset and limit
        $products = Card::with('brands:brand_name,card_id')
            ->with('product_type:type,card_id')
            ->with('cover:image,card_id')
            ->with('fields:name_field,description_field,card_id')
            ->select('cards.card_id', 'cards.title', 'cards.brief_information', 'cards.cost')
            ->orderBy('cards.created_at');

        if (isset($offset) && isset($limit)) {
            $products = $products->offset($offset)->limit($limit);
        }

        $products = $products->get();

        return $products;
    }

    /**
     * callable in controller for getting products after filtered by brand name
    */
    public function getProductsByBrandName(Request $request, string $brand, int $offset = NULL, int $limit = NULL)
    {
        $offset = $request->offset;
        $limit = $request->limit;

        # check have or not comma in query
        $commaCheck = preg_match_all('/,/', $brand);

        # check one or more elements after choice brands
        $brand = !empty($commaCheck) ? explode(',', $brand) : array($brand);

        # check have or not offset and limit
        $products = Card::with('brands:brand_name,card_id')
            ->with('product_type:type,card_id')
            ->with('cover:image,card_id')
            ->with('fields:name_field,description_field,card_id')
            ->select('cards.card_id', 'cards.title', 'cards.brief_information', 'cards.cost')
            ->whereIn('brands.brand_name', $brand)
            ->orderBy('cards.created_at');

        if (isset($offset) && isset($limit)) {
            $products = $products->offset($offset)->limit($limit);
        }

        $products = $products->get();

        return $products;
    }

    /**
     * callable in controller and getting products by type (smartphone or TV or anything else)
    */
    public function getProductsByProductType(Request $request, string $type, int $offset = NULL, int $limit = NULL)
    {
        $offset = $request->offset;
        $limit = $request->limit;

        # check have or not comma in query
        $commaCheck = preg_match_all('/,/', $type);

        # check one or more elements after choice brands
        $type = !empty($commaCheck) ? explode(',', $type) : array($type);

        # check have or not offset and limit
        $products = Card::with('brands:brand_name,card_id')
            ->with('product_type:type,card_id')
            ->with('cover:image,card_id')
            ->with('fields:name_field,description_field,card_id')
            ->select('cards.card_id', 'cards.title', 'cards.brief_information', 'cards.cost')
            ->whereIn('product_type.type', $type)
            ->orderBy('cards.created_at');

        if (isset($offset) && isset($limit)) {
            $products = $products->offset($offset)->limit($limit);
        }

        $products = $products->get();
        return $products;
    }

    /**
     * method for getting recommendation products
    */
    public function getRecommendation(Request $request, $id)
    {
        $model = Card::findOrFail($id);
        $cost = $this->getArrayRecommendCosts($model);

        $recommendations = Card::where('cards.card_id', '!=', $model->card_id)
            ->whereIn('cost', $cost)
            ->leftJoin('product_type', 'product_type.card_id', '=', 'cards.card_id')
            ->where('product_type.type', '=', $model->product_type->type)
            ->leftJoin('brands', 'brands.card_id', '=', 'cards.card_id')
            ->leftJoin('cover', 'cover.card_id', '=', 'cards.card_id')
            ->get();

        return $recommendations;
    }


    # get costs +- 2k
    public function getArrayRecommendCosts(object $product)
    {
        # get cost from model
        $cost = $product->cost;
        $cost = intval($cost);

        $copyCost = $cost;
        $arrayWithRecommendations[] = $cost;

        for ($i = 0, $count = 2; $i < $count; $i++) {
            $arrayWithRecommendations[] = intval($copyCost) - 1000;
            $copyCost = intval($copyCost) - 1000;
        }

        for ($i = 0, $count = 2; $i < $count; $i++) {
            $arrayWithRecommendations[] = intval($cost) + 1000;
            $cost = intval($cost) + 1000;
        }
        return $arrayWithRecommendations;
    }

    /**
     * data for create new object in CardController
    */
    public function dataToCreate($request)
    {
        $dataToSave = [
            'title' => $request->title, 'brief_information' => $request->brief_information, 'cost' => $request->cost,
            'additional_information' => $request->additional_information
        ];
        return $dataToSave;
    }

    /**
     * data to update
    */
    public function dataToUpdate(Request $request, $model)
    {
        $dataToSave = [
            'title' => $request->title ? $request->title : $model['title'],
            'brief_information' => $request->brief_information ? $request->brief_information : $model['brief_information'],
            'cost' => $request->cost ? $request->cost : $model['cost'],
            'additional_information' => $request->additional_information
        ];
        return $dataToSave;
    }

    /**
     * relation has one brand in table brands
    */
    public function brands()
    {
        return $this->hasOne('App\Brand', 'card_id');
    }

    /**
     * relation has many images in cover
    */
    public function cover()
    {
        return $this->hasMany('App\Cover', 'card_id');
    }

    /**
     * relation has one type in product_types
    */
    public function product_type()
    {
        return $this->hasOne('App\ProductType', 'card_id');
    }

    /**
     * relation has many fields in field table
    */
    public function fields()
    {
        return $this->hasMany('App\Field', 'card_id');
    }
}
