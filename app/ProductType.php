<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $table = 'product_type';

    protected $primaryKey = 'product_type_id';

    protected $fillable = ['product_type_id', 'type', 'card_id'];

    /**
     * relation for Product. This table have product_types (smartphone, TV, notebooks and other)
     */
    public function cards()
    {
        return $this->belongsTo('App\Card', 'card_id');
    }
}
