<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:100'],
            'email' => ['required', 'max:50', 'email', 'unique:users'],
            'password' => ['required', 'max:10', 'min:8', 'confirmed']
        ];
    }

    # Messages for response
    public function messages()
    {
        return [
            'name.required' => 'Необходимо указать Ваше имя.',
            'email.required' => 'Необходимо указать Вашу электронную почту.',
            'password.required' => 'Необходимо указать Ваш пароль.',

            'name.max' => 'Имя должно составлять не более 100 символов.',
            'email.max' => 'Электронная почта должна иметь не более 100 символов.',
            'password.max' => 'Пароль должно иметь не более 10 символов.',

            'email.email' => 'Электронная почта должена быть настоящей.',
            'email.unique' => 'Пользователь с такой электронной почной уже зарегистрирован.',

            'password.min' => 'Пароль должен составлять не мение 8 символов.',
            'password.confirmed' => 'Неправильное подтверждение пароля.'
        ];
    }
}
