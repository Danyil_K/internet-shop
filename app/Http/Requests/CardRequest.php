<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;

class CardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $admin = User::where('email', 'admin@gmail.com')->first();
        if ($admin)
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'max:200'],
            'brand_name' => ['required', 'max:100'],
            'type' => ['required', 'max:100'],
            'image' => ['required', 'mimes:jpeg,png,jpg,gif,svg', 'max:4000'],
            'brief_information' => ['required', 'max:450'],
            'cost' => ['required', 'max:10'],
            'additional_information' => ['max:450']
        ];
    }

    # Messages for response
    public function messages()
    {
        return [
            'title.required' => 'Необходимо указать название.',
            'brief_information.required' => 'Необходимо указать описание.',
            'cost.required' => 'Необходимо указать цену товара.',
            'brand_name.required' => 'Необходимо указать название бренда.',
            'type.required' => 'Необходимо выбрать тип товара (телефон, телевизор, компьютер или ноутбук).',
            'image.required' => 'Необходимо выбрать изображение.',
            'name_field.required' => 'Необходимо заполнить название поля.',
            'description_field.required' => 'Необходимо дать описание полю.',

            'title.max' => 'Поле должно быть не более 200 символов.',
            'brief_information.max' => 'Поле должно быть не более 450 символов.',
            'brand_name.max' => 'Поле должно быть не более 100 символов.',
            'type.max' => 'Поле должно быть не более 100 символов.',
            'image.max' => 'Изображени должно быть размером менее 4МБ.',
            'cost.max' => 'Поле должно быть не более 10 символов.',
            'image.mimes' => 'Изображение должно быть формата - jpeg, png, jpg, gif или svg.',
            // 'name_field.max' => 'Поле должно быть не более 200 символов.',
            // 'description_field.max' => 'Поле должно быть не более 450 символов.',
            'additional_information.max' => 'Поле должно быть не более 450 символов.'
        ];
    }
}
