<?php

namespace App\Http\Controllers;

use App\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{
    /**
     * get types from table product_type
    */
    public function getProductTypes()
    {
        $product_types = ProductType::select('product_type.type')->get();

        return response()->json($product_types, 200);
    }
}
