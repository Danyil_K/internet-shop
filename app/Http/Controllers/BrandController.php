<?php

namespace App\Http\Controllers;

use App\Repository\BrandRepository;

class BrandController extends Controller
{
    private $brandRepository;

    public function __construct()
    {
        $this->brandRepository = app(BrandRepository::class);
    }

    public function getBrands(): object
    {
        (object) $brands = $this->brandRepository->getBrands();

        return response()->json($brands, 200);

    }


    public function getOneBrand(int $id): object
    {
        (object) $brand = $this->brandRepository->getOneBrand($id);

        return response()->json($brand, 200);
    }


    public function getIdAndNameBrands(): object
    {
        (object) $brands = $this->brandRepository->getIdAndNameBrands();

        return response()->json($brands, 200);
    }


    public function getBrandsWithCards(): object
    {
        (object) $brandsWithCards = $this->brandRepository->getBrandsWithCards();

        return response()->json($brandsWithCards, 200);
    }
}
