<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRequest;

class SignController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function register(UserRequest $request)
    {
        $request->validate([
            'name' => ['required', 'max:100'],
            'email' => ['required', 'max:50', 'email', 'unique:users'],
            'password' => ['required', 'min:8', 'max:10', 'confirmed']
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'message' => 'Новый пользователь успешно зарегестрирован.'
        ], 201);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);
        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['Электронная почта или пароль введены не верно.']
            ]);
        }

        return response()->json($user->createToken('Auth Token')->accessToken, 200);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Выход с учётной записи выполнен удачно.'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function getUser(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * Update user info
    */
    public function update(Request $request)
    {
        $user = $request->user();

        $request->validate([
            'name' => ['max:100'],
            'email' => ['max:50', 'email', 'unique:users'],
            'password' => ['min:8', 'max:10', 'confirmed']
        ]);

        if ( !empty($request->password) ) {
            $password = Hash::make($request->password);
        } else {
            $password = $user->password;
        }

        $dataToSave = [
            'name' => $request->name ? $request->name : $request->user()->name,
            'email' => $request->email ? $request->email : $request->user()->email,
            'password' => $password
        ];

        $user->update($dataToSave);

        return response()->json([
            'message' => 'Ваши данные обновлены.'
        ], 201);

    }
}
