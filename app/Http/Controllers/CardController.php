<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Card;
use App\Cover;
use App\Brand;
use App\Field;
use App\ProductType;
use App\Http\Requests\CardRequest;

class CardController extends Controller
{
    /**
     * create one card
    */
    public function create(CardRequest $request)
    {
        $model = new Card();

        $card = Card::create($model->dataToCreate($request));

        // check has img in the request or not
        $image = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('uploads', 'public');
            $image = 'storage/' . $image;
        }
        $fields = json_decode($request->fields);

        foreach ($fields as $key => $val) {
            Field::create([
                'name_field' => $fields[$key]->name_field,
                'description_field' => $fields[$key]->description_field,
                'card_id' => $card->card_id
            ]);
        }

        Brand::create(['brand_name' => $request->brand_name, 'card_id' => $card->card_id]);
        ProductType::create(['type' => $request->type, 'card_id' => $card->card_id]);
        Cover::create(['image' => $image, 'card_id' => $card->card_id]);

        return response()->json($card, 201);
    }

    /**
     * get all products
    */
    public function getProducts(Request $request, int $offset = NULL, int $limit = NULL)
    {
        # TODO for admin this methods
        $model = new Card();

        $model = $model->getProductsAllInfo($request, $offset, $limit);

        return response()->json($model, 200);
    }

    /**
     * get products by brand
    */
    public function getByBrandName(Request $request, string $brand, int $offset = NULL, int $limit = NULL)
    {
        $model = new Card();

        $model = $model->getProductsByBrandName($request, $brand, $offset, $limit);

        return response()->json($model, 200);
    }

    /**
     * get products by product_type
    */
    public function getByProductType(Request $request, string $type, int $offset = NULL, int $limit = NULL)
    {
        $model = new Card();

        $model = $model->getProductsByProductType($request, $type, $offset, $limit);

        return response()->json($model, 200);
    }

    /**
     * update products
    */
    public function updateCard(Request $request, int $id)
    {
        $model = Card::find($id);
        $main_image = Cover::getImage($model);
        $brand_name = $model->brands;
        $type = $model->product_type;
        $fields = $model->fields;

        $dataToSave = $model->dataToUpdate($request, $model);

        if ($request->hasFile('image')) {
            if (Storage::disk('public')->exists($main_image) )  {
                Storage::disk('public')->delete($main_image);
            }
            $pathImg = $request->file('image')->store('uploads', 'public');
            $main_image = 'storage/' . $pathImg;
        }

        // update Cards
        $model->update($dataToSave);
        // update Brands
        $model->brands->update(['brand_name' => $request->brand_name ? $request->brand_name : $brand_name->brand_name]);
        // update Product_type
        $model->product_type->update(['type' => $request->type ? $request->type : $type->type]);
        // update Cover [0] - because oneToMany
        $model->cover[0]->update(['image' => $main_image]);
        // update Field
        $decode_fields = json_decode($request->fields);
        foreach ($decode_fields as $key => $value) {
            $model->fields[$key]->update([
                'name_field' => $value->name_field ? $value->name_field : $fields[$key]->name_field,
                'description_field' => $value->description_field ? $value->description_field : $fields[$key]->description_field
            ]);
        }

        return response()->json($model, 201);
    }

    /**
     * delete products
    */
    public function deleteCard(int $id)
    {
        $model = Card::find($id);

        if (empty($model)) return response()->json(null, 202);

        $main_image = Cover::getImage($model);

        if (Storage::disk('public')->exists($main_image)) {
            Storage::disk('public')->delete($main_image);
        }

        $model->delete();

        return response()->json('deleted', 200);
    }

    /**
     * get products recommendations
    */
    public function getRecommendations(Request $request, $id)
    {
        $model = new Card();

        $model = $model->getRecommendation($request, $id);

        return response()->json($model, 200);
    }
}
