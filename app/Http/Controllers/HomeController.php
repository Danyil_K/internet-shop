<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;
use App\Card;
use App\ProductType;

/**
 * Get this class to client
*/
class HomeController extends Controller
{
    /**
     * get products
    */
    public function getProducts(Request $request, int $offset = NULL, int $limit = NULL)
    {
        $model = new Card();
        $model = $model->getProductsAllInfo($request, $offset, $limit);

        return response()->json($model, 200);
    }

    /**
     * get products by brand name
    */
    public function getByBrandName(Request $request, string $brand, int $offset = NULL, int $limit = NULL)
    {
        $model = new Card();
        $model = $model->getProductsByBrandName($request, $brand, $offset, $limit);

        return response()->json($model, 200);
    }

    /**
     * get products by product type
    */
    public function getByProductType(Request $request, string $type, int $offset = NULL, int $limit = NULL)
    {
        $model = new Card();
        $model = $model->getProductsByProductType($request, $type, $offset, $limit);

        return response()->json($model, 200);
    }

    /**
     * get brands
    */
    public function getBrands()
    {
        $model = Brand::select('brands.brand_name')->get();

        return response()->json($model, 200);
    }

    /**
     * get product types
    */
    public function getProductTypes()
    {
        $model = ProductType::select('product_type.type')->get();

        return response()->json($model, 200);
    }
}
