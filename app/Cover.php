<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cover extends Model
{
    protected $table = 'cover';

    protected $primaryKey = 'cover_id';

    protected $fillable = ['cover_id', 'image', 'card_id'];

    public static function getImage($model) {

        $image = $model->cover;
        $image = $image[0]['image'];
        $imageWithoutPath = str_replace('storage/', '', $image);

        return $imageWithoutPath;
    }

    // relation this table has many images for Products
    public function cards() {
        return $this->belongsToMany('App\Card', 'card_id');
    }
}
