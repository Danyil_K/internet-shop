<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $table = 'fields';

    protected $primaryKey = 'field_id';

    protected $fillable = ['field_id', 'name_field', 'description_field', 'card_id'];

    /**
     * relation one to one with Products
    */
    public function cards() {
        return $this->belongsToMany('App\Card', 'card_id');
    }
}
