<?php

namespace App\Repository;

use App\Brand as Model;

class BrandRepository extends CoreRepository
{
    protected function getModelClass()
    {
        return Model::class;
    }


    public function getBrands(): object
    {
        return $this
            ->startConditions()
            ->select('brands.brand_name')
            ->get();
    }


    public function getOneBrand($id): object
    {
        return $this
            ->startConditions()
            ->findOrFail($id);
    }

    public function getIdAndNameBrands(): object
    {
        $result = $this
            ->startConditions()
            ->selectRaw('brands.*, CONCAT (brands.brand_id, " - ", brands.brand_name) as id_name')
            ->toBase()
            ->get();

        return $result;
    }

    public function getBrandsWithCards(): object
    {
        $result = $this
            ->startConditions()
            ->selectRaw('brands.brand_name, concat (sum(cards.cost), " grn") as brand_cost, count(brand_name) as count')
            ->join('cards', 'cards.card_id', 'brands.card_id')
            ->groupBy('brands.brand_name')
            ->toBase()
            ->get();
        return $result;
    }
}
